/* 
Libsynth - A signal synthesis library	
Copyright (C) 2018,2019 Ryan Medeiros

    Libsynth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Libsynth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsynth.  If not, see <https://www.gnu.org/licenses/>.
    
You may contact me at lofenyy (at) linuxmail (dot) org. 
*/

//Please read the provided HTML documentation. 

/* Index:
	1) Generators
		1.1) PWM Sine
		1.2) Noise
		1.3) Sawtooth
	2) Wave-to-wave filters
		2.1) Sine to Pulse/Square
		2.2) Sine to Triangle
	3) Utilities
		3.1) Amplifier (planned)
		3.2) Envelope Generator
		3.3) Note to frequency calculator
	4) Frequency filters (planned)
*/

#include<stdlib.h>
#include<math.h>

// 1) Basics
//	1.1) Oscillator
double synth_Sine(double increment, double shape, double pitch, double sample_rate);

//	1.2) Noise
double synth_Noise();

//	1.3) Sawtooth
double synth_Sawtooth(double increment, double pitch, double sample_rate);

// 2) Wave-to-wave filters
//	2.1) Sine to Pulse/Square
double synth_Sin2Square(double input);

//	2.2) Sine to Triangle
double synth_Sin2Triangle(double input);

// 3) Utilities
//	3.1) Envelope Generator
double synth_Env(double increment, double starttime, double startvol, double stopvol, double duration, double sample_rate);

//	3.2) Note to frequency calculator
double note2freq(char* note, int octave);

#include"libsynth.c"
