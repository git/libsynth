/* 
Libsynth - A signal synthesis library	
Copyright (C) 2018,2019 Ryan Medeiros

    Libsynth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Libsynth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with libsynth.  If not, see <https://www.gnu.org/licenses/>.
    
You may contact me at lofenyy (at) linuxmail (dot) org. 
*/
double synth_Sine(double increment, double shape, double pitch, double sample_rate)
	{
	double nincrement = fmod(increment, sample_rate /pitch);
	
	if(nincrement < ((sample_rate *shape) /pitch))
		{
		return sin( (2 *M_PI *pitch *increment *( 1/(shape*2) )) / (sample_rate) );
		}
	else
		{
		nincrement = nincrement -((sample_rate *shape) /pitch);
		return -sin( (2 *M_PI *pitch *nincrement *(1/(2*(1-shape))) ) / (sample_rate) );
		}
	}

double synth_Noise()
	{
	//Converts output of rand (0 to RAND_MAX) to double (-1 to 1)
	return ((double)rand() / (double)(RAND_MAX/2)) -1;
	}

//To be revised
double synth_Sawtooth(double increment, double pitch, double sample_rate)
	{
	return 2* ( (increment*pitch /sample_rate) - floor((increment*pitch) /sample_rate) -0.5);
	}

double synth_Sin2Square(double X)
	{
	//Self explanitory?
	if(X > 0)
		{
		return 1;
		}
	else if(X < 0)
		{
		return -1;
		}
	else 
		{
		return 0;
		}
	}

double synth_Sin2Triangle(double X)
	{
	//The function for a triangle wave is "isin(sin(2*pi*x)) /2pi". Since
	//Oscil is the input, X = sin(2*pi*X). When we simplify our function,
	//we get:
	return 2*asin(X) /M_PI;
	}

double synth_Env(double increment, double starttime, double startvol, double stopvol, double duration, double sample_rate)
	{
	//If the envelope isn't supposed to run, be completely open.
	if((increment/sample_rate) < starttime)
		{
		return 1;
		}
	
	//I was so proud of how well documented this was when I first wrote
	//this and now I'm crying...
	double C = (increment-(starttime*sample_rate)) /(duration*sample_rate);
	
	if( C < 1)
		{
		return C*(stopvol-startvol) +startvol;
		}
	else
		{
		//When we're no longer used, remain in this state. 
		return stopvol;
		}
	}
	
double note2freq(char* note, int octave)
	{
	double steps = 12*(octave-4) -9;
	
	if(strcmp(note,"C") == 0)
		{
		steps = steps +0;
		}
	else if(strcmp(note,"C#") == 0 || strcmp(note,"Db") == 0)
		{
		steps = steps +1;
		}
	else if(strcmp(note,"D") == 0)
		{
		steps = steps +2;
		}
	else if(strcmp(note,"D#") == 0 || strcmp(note,"Eb") == 0)
		{
		steps = steps +3;
		}
	else if(strcmp(note,"E") == 0)
		{
		steps = steps +4;
		}
	else if(strcmp(note,"F") == 0)
		{
		steps = steps +5;
		}
	else if(strcmp(note,"F#") == 0 || strcmp(note,"Gb") == 0)
		{
		steps = steps +6;
		}
	else if(strcmp(note,"G") == 0)
		{
		steps = steps +7;
		}
	else if(strcmp(note,"G#") == 0 || strcmp(note,"Ab") == 0)
		{
		steps = steps +8;
		}
	else if(strcmp(note,"A") == 0)
		{
		steps = steps +9;
		}
	else if((strcmp(note,"A#") == 0) || (strcmp(note,"Bb") == 0))
		{
		steps = steps +10;
		}
	else if(strcmp(note,"B") == 0)
		{
		steps = steps +11;
		}
	else
		{
		return 0;
		}
	
	return 440*pow( pow(2, 1/12.0 ), steps );
	}
